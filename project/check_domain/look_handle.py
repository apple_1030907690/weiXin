# -*- coding: utf-8 -*-

'''
2019年10月24日11:51:50
句柄捕获
'''

'''
#回车
win32api.keybd_event(13, 0, 0, 0)
#关闭窗口
win32gui.PostMessage(handle, win32con.WM_CLOSE, 0, 0)
'''

'''
#发送键位置
win32api.SetCursorPos([right - 47, bottom - 15])
# 执行左单键击，若需要双击则延时几毫秒再点击一次即可
win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP | win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)

'''


import win32gui
import win32con
import win32api
from pymouse import PyMouse
from pykeyboard import PyKeyboard
import time
from PIL import ImageGrab
import pytesseract




jbid = 1312016;
k = PyKeyboard()

def get_child_windows(parent):
    '''
    获得parent的所有子窗口句柄
     返回子窗口句柄列表
     '''
    if not parent:
        return
    hwndChildList = []
    win32gui.EnumChildWindows(parent, lambda hwnd, param: param.append(hwnd),  hwndChildList)
    return hwndChildList

def check_domain(domain):


    startTime = time.time();
    # 前置窗口
    #win32gui.SetForegroundWindow(jbid)
    handle = win32gui.FindWindow("WeChatMainWndForPC", "微信")

    # 获取窗口位置
    left, top, right, bottom = win32gui.GetWindowRect(handle)
    #print("left " + str(left) + " top " + str(top) + " right " + str(right) + " boottom " + str(bottom))
    # 获取某个句柄的类名和标题
    # title = win32gui.GetWindowText(handle)
    # clsname = win32gui.GetClassName(handle)

    # 移动到输入框
    win32api.SetCursorPos([right - 100, bottom - 100])
    # 执行左单键击，若需要双击则延时几毫秒再点击一次即可
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP | win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
    # 被封的网站
    #k.type_string('http://dwc.ccac7.com/goods/wechatPublicPayPage')
    # 未被封的网站
    #k.type_string('https://www.hfysg.com')
    # k.type_string('https://blog.csdn.net/p358278505/article/details/79101832')
    k.type_string(domain)


    # 回车
    win32api.keybd_event(13, 0, 0, 0)

    time.sleep(1)

    #计算高度
    diffBottom = 185
    if len(domain) > 115:
        print(domain + " >115")
        diffBottom = 188;

    # 鼠标移动到历史消息窗口
    win32api.SetCursorPos([right - 145, bottom - diffBottom])
    # 执行左单键击，若需要双击则延时几毫秒再点击一次即可
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP | win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)

    time.sleep(3);
    chile_handle = win32gui.FindWindow("CefWebViewWnd", "微信")
    #  裁剪得到全图
    game_rect = win32gui.GetWindowRect(chile_handle)
    src_image = ImageGrab.grab(game_rect)
    # src_image.save('accessed.png')
    # 关闭窗口
    win32gui.PostMessage(chile_handle, win32con.WM_CLOSE, 0, 0)
    text = pytesseract.image_to_string(src_image, lang='eng')
    #print(text);
    if text is None or text.find('This page cannot be accessed now.') > 0:
        print(domain +' 已被封')
    else:
        print(domain + ' 域名可以使用')

    endTime = time.time();
    diffTime = endTime - startTime;
    print('执行时间' + str(diffTime));





if __name__ == '__main__':

    time.sleep(5);
    domain_list = ['https://blog.csdn.net/p358278505/article/details/79101832details/79101832details/79101832details/79101832details/79',
                   'https://www.ffff.com',
                   'http://dwc.ccac7.com/goods/wechatPublicPayPage'
                   ,'http://192.168.0.55:81/zentao/www/user-login-L3plbnRhby93d3cv.html'];
    for item in domain_list:
        check_domain(item)

