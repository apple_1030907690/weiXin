'''
python 模拟鼠标点击和键盘输入


pip install pywin32

pywin32模块默认已安装

pyHook模块可从这里下载
pip install PyHook3
或者
http://www.lfd.uci.edu/~gohlke/pythonlibs/
//在python官网找了很多个pyHook都不适用于python3.5版本

PyUserInput模块
https://github.com/PyUserInput/PyUserInput
'''
from pymouse import PyMouse
from pykeyboard import PyKeyboard
import time

if __name__ == '__main__':
    m = PyMouse()
    k = PyKeyboard()
    x_dim, y_dim = m.screen_size()
    print(str(x_dim)+"---" + str(y_dim))
    time.sleep(5);
    m.click(986, 892, 1)
    k.type_string('https://blog.csdn.net/p358278505/article/details/79101832')
    m.click(1316, 874, 1)
    m.click(1275, 726, 1),