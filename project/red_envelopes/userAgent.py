# -*- coding: utf-8 -*-

'''
抓取 user-agent
文档 https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html#id10
zhouzhongqing
2018年7月10日22:06:48
'''

import requests;
from bs4 import BeautifulSoup


# 写入文件
def write_to_file(file_name, txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件" + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读
    
    'w'：写
    
    'a'：追加
    
    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）
    
    'w+' == w+r（可读可写，文件若不存在就创建）
    
    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'a+', encoding='utf-8');
    # 2 读写文件
    f.write(str(txt));
    # 3 关闭文件
    f.close();


'''
发起请求
@:param url
'''

#为了去重
userAgentList = [];


def requestUserAgent(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'};
    result = requests.post(url, data={}, verify=False);
    # print(result.text);

    # 初始化并制定解析器
    soup = BeautifulSoup(result.text, "lxml");
    # 得到table
    table = soup.table;
    # print(table);
    tr_arr = table.find_all("tr");
    for tr in tr_arr:
        # //查询所有td
        tds = tr.find_all('td');
        # for  td in tds:
        # 得到User-Agent字符串
        # print(tds[3].get_text());
        userAgent = tds[3].get_text();
        if "User-Agent字符串" != userAgent :
            user_agent_build = userAgent.split("Build/")[0];
            user_agent_arr = user_agent_build.split(";");
            phoneModel = user_agent_arr[len(user_agent_arr) - 1];
            print(phoneModel.strip());
            #排重
            if phoneModel.strip() not in userAgentList:
                write_to_file("d:/vivo.txt","androidShop.set(\""+phoneModel.strip()+"\",vivoDownloadAddress);\n")
                userAgentList.append(phoneModel.strip());


if __name__ == '__main__':
    print("start");
    '''
    userAgent = "Mozilla/5.0 (Linux; Android 6.0; vivo Y67A Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/35.0.1916.138 Mobile Safari/537.36 T7/7.4 baiduboxapp/8.5 (Baidu; P1 6.0)";
    user_agent_build = userAgent.split("Build/")[0];
    user_agent_arr = user_agent_build.split(";");
    phoneModel = user_agent_arr[len(user_agent_arr) - 1];
    print(phoneModel.strip())
    '''

    for i in range(1, 83):
         print("这是第"+str(i)+"页");
         #vivo的机型
         requestUserAgent("http://www.fynas.com/ua/search?b=&d=vivo&page="+str(i));

    '''
    for i in range(1, 84):
        print("这是第" + str(i) + "页");
        # 小米的机型
        requestUserAgent("http://www.fynas.com/ua/search?d=%E5%B0%8F%E7%B1%B3&b=&page=" + str(i));
    '''

    print("end");
