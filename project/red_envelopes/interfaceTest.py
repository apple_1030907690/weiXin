# -*- coding: utf-8 -*-

'''
红包项目接口测试
'''

import requests
import json

import os

import time
import threading

# 调用jar
import jpype

'''
得到token接口
'''


def getToken(client_id):
    result = requests.post("http://127.0.0.1:8083/jpush/getToken?", data={"id": client_id}, verify=False);
    token = result.text;
    return token;


'''
发送红包接口
'''


def send_packet(token):
    '''
    发送红包接口
    '''
    request_url1 = "https://api.xx.cn/api/packet/send_packet";
    headers = {'Content-type': 'application/json;charset=utf-8',
               'token': token};
    data1 = {"amount": "1",
             "city": "重庆市",
             "distance_type": "2",
             "gender": 2,
             "lat": "29.531092",
             "lon": "106.566879",
             "money": "1",
             "packet_text": "",
             "packet_title": "",
             "packet_type": "0",
             "province": "重庆市",
             "region": "南岸区"
             };

    result1 = requests.post(request_url1, headers=headers, data=json.dumps(data1), verify=False);
    print(result1.text);


class myThread(threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q

    def run(self):
        print("Starting " + self.name)
        process_data(self.name, self.q);
        print("Exiting " + self.name);


def process_data(threadName, q):
    for i in range(500000):
        send_packet(token);
        time.sleep(1);


if __name__ == '__main__':
    print("start ");

    # 获取token接口
    client_id = 1;
    token = getToken(client_id);
    print("得到token ", token);

    # send_packet(token);


    threadID = 1;
    threads = []
    # 创建新线程
    for tName in range(500):
        thread = myThread(threadID, "Thread-" + str(tName), threadID)
        thread.start();
        threads.append(thread)
        threadID += 1

print("end ");
