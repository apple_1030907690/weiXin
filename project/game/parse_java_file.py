# -*- coding: utf-8 -*-
'''

解析java文件 生成java属性的xml
zhouzhongqing
2019年5月22日10:29:02
'''

import os

'''
逐行读取文件
'''


def read_file(filePath):
    file_line_list = [];
    file = open(filePath, "r", encoding='UTF-8')

    while 1:
        line = file.readline();
        if line.strip("\n") != "":
            file_line_list.append(line);
        if not line:
            break
        pass  # do something
    return file_line_list;


def print_table_xml(basePath):
    for base_path_item in os.listdir(basePath):
        if os.path.isfile(basePath + base_path_item):
            # print("file "+base_path_item);
            if os.path.exists(basePath + base_path_item):
                file_line_list = read_file(basePath + base_path_item);
                tableId = "";
                column_list = [];
                for index in range(len(file_line_list)):
                    # print('当前数据 :', file_line_list[index]);
                    # 得到package
                    if file_line_list[index].startswith("package"):
                        # print("package = " + file_line_list[index].strip("package").strip().strip(";"));
                        packageStr = file_line_list[index].strip("package").strip().strip(";");

                    # 得到类名
                    if file_line_list[index].startswith("public class "):
                        className = file_line_list[index];
                        classNameArray = str(className).split(" ");
                        tableId = packageStr + "." + classNameArray[2];
                        # print("tableId: " + tableId);
                    # 得到他的全部属性
                    if file_line_list[index].strip().startswith("private "):
                        replace_list = ["privateString", "privateInteger", "privateLong", "privateDate", "privateint",
                                        "privatelong","=newInteger(0)","=newInteger(1)","privateTimestamp","privateList<String>",
                                        "privateList<Integer>", "privateList<Long>", "privateList<GameConnectorEntity>",
                                        "=newArrayList<GameConnectorEntity>()",
                                        "privateMap<Long,AlipayEntity>", "privateMap<Long,BankEntity>",
                                        "privateMap<Long,Long>", "privateMap<Long,AlipayEntity>"
                                        ];
                        filed_value = file_line_list[index].strip().replace(' ', '').strip(";");
                        # 替换
                        for replace_list_item in replace_list:
                            filed_value = filed_value.replace(replace_list_item, "");
                        # print(filed_value);
                        column_list.append(filed_value);

            # 生成xml文件
            tableStr = "";
            tableStr += "<table  id=\"" + tableId + "\" description=\"" + tableId + "\" >\n";
            tableStr += "\t<column  id=\"id\" description=\"id\" ></column>\n";
            tableStr += "\t<column  id=\"createTime\" description=\"创建时间\" ></column>\n";
            tableStr += "\t<column  id=\"updateTime\" description=\"修改时间\" ></column>\n";
            tableStr += "\t<column  id=\"dbConfig\" description=\"dbConfig\" ></column>\n";
            tableStr += "\t<column  id=\"channelId\" description=\"渠道号\" ></column>\n";
            for column_list_item in column_list:
                if column_list_item.startswith("privatestaticfinallongserialVersionUID") == False:
                    tableStr += "\t<column id=\"" + column_list_item + "\" description=\"" + column_list_item + "\"></column>\n";
            tableStr += "</table>\n";
            print("\n" + tableStr);


if __name__ == '__main__':
    print("start");
    base_path_array = ["F:/work/red_package/red-packet-server/common-db/src/main/java/com/lyh/game/mongo/game/domain/",
                       "F:/work/red_package/red-packet-server/common-db/src/main/java/com/lyh/game/mongo/game/data/",
                       "F:/work/red_package/red-packet-server/common-db/src/main/java/com/lyh/game/mongo/game/domain/config/",

                       "F:/work/red_package/red-packet-server/mongo-logs/src/main/java/com/lyh/game/mongo/admin/",
                       "F:/work/red_package/red-packet-server/mongo-logs/src/main/java/com/lyh/game/mongo/logs/"
                       ];
    # fileName = "AgentWithdrawRecordLog.java";
    for basePath in base_path_array:
        print_table_xml(basePath);

    print("end");
