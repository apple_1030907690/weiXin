# -*- coding: utf-8 -*-

'''
识别游戏的版本图片



'''

from PIL import Image
import pytesseract
import os
import re
import time
import json


# 通过抛出异常 判断是否是纯数字
def is_num_by_except(num):
    try:
        float(num);
        return True
    except ValueError:
        #        print "%s ValueError" % num
        return False


# 写入文件
def write_to_file(file_name, txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件" + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读

    'w'：写

    'a'：追加

    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）

    'w+' == w+r（可读可写，文件若不存在就创建）

    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'a+', encoding='utf-8');
    # 2 读写文件
    f.write(str(txt));
    # 3 关闭文件
    f.close();


if __name__ == '__main__':
    '''
    sql语句:
        db.game_config.update({"_id" : 8007},{"$set":{"gameVersion":"0.01"}});
    '''

    print(" start ");

    hall_version_str = "大厅强制更新版本号";

    hall_hot_version_str = "大厅热更新版本号";

    # sql语句
    update_sql_array = [];
    # 更新的游戏和版本

    line = 0;

    # update_sql_array.append("db.game_config.update({\"_id\""+":"+str(game_id_name[0])+"},{\""+"$set\":{\"gameVersion\":\""+game_version_value+"\"}});");

    context = open("./child_game_version.json", "r");
    text = context.read();

    jsonArray = json.loads(text);
    for item in jsonArray:
        line = line + 1;
        gameVersion = str(item["gameVersion"]);
        id = str(item["id"]);
        update_sql_array.append(
            "db.game_config.update({\"_id\"" + ":" + id + "},{\"" + "$set\":{\"gameVersion\":\"" + gameVersion + "\"}});");

    print("更新的大概行数 ： " + str(line));

    save_path = "F:/work/package/child_game_version/";
    ticks = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
    for sql_item in update_sql_array:
        print(sql_item);
        write_to_file(save_path + ticks + ".txt", sql_item + "\n");



    print(" end ");

    random_input = input("请输入任意字符串结束.");
    os.system("explorer file:\\\/"+ save_path+ticks+".txt");
