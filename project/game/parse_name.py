# -*- coding: utf-8 -*-


'''
解析名称
http://www.resgain.net/xmdq.html
'''
from bs4 import BeautifulSoup
import requests;

def name_list(url="http://zhao.resgain.net/name_list_1.html"):
    content = requests.get(url);
    # print(content.text);
    # 初始化并制定解析器
    soup = BeautifulSoup(content.text, "lxml");
    col_xs_12_arr = soup.find_all(class_='col-xs-12');
    # print(col_xs_12_arr[2].get_text());

    for text in col_xs_12_arr[2]:
        soup = BeautifulSoup(str(text), "lxml");
        print(soup.get_text());
        write_to_file("d:/name_list.txt", soup.get_text()+" ");




# 写入文件
def write_to_file(file_name, txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件" + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读

    'w'：写

    'a'：追加

    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）

    'w+' == w+r（可读可写，文件若不存在就创建）

    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'a+', encoding='utf-8');
    # 2 读写文件
    f.write(str(txt));
    # 3 关闭文件
    f.close();

if __name__ == '__main__':
    print("start");

    for i in range(1,11):
        print("-----------------第 {" +str(i)+"} 页-----------------" )
        name_list("http://zhao.resgain.net/name_list_"+str(i)+".html");



    print("end");