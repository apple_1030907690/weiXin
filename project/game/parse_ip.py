# -*- coding: utf-8 -*-

'''

解析地址库
'''


# 写入文件
def write_to_file(file_name, txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件" + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读

    'w'：写

    'a'：追加

    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）

    'w+' == w+r（可读可写，文件若不存在就创建）

    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'a+', encoding='utf-8');
    # 2 读写文件
    f.write(str(txt));
    # 3 关闭文件
    f.close();


if __name__ == '__main__':
    print("start");
    file_object = open('D:/software/cz88.net/ip/ip_list2.txt', 'r');
    txtLine = 0;

    text = "";
    try:
        for line in file_object:
            strAarr = line.split(" ", 100);
            addressName = strAarr[len(strAarr) - 2];
            # find 从下标0开始，查找在字符串里第一个出现的子串 返回下标
            #print(addressName + "---" + str(addressName.find("市")) );
            if strAarr[0] != "" and strAarr[len(strAarr) - 2] != "" and "IANA保留地址" != strAarr[len(strAarr) - 2] and  addressName.find("市") >  0 :
                addressNameLength = len(addressName);
                #print(addressName + "---"+ str(addressNameLength));
                # 城市小于5个长度
                if addressNameLength <= 5:
                    #print(addressName + "----" + str(strAarr[0]));
                    txtLine = txtLine + 1;
                    sqlText = "{\"_id\":{\"$numberLong\":\""+str(txtLine)+"\"},\"ipAddress\" : \""+str(strAarr[0])+"\", \"dbConfig\" : 1 } \n";
                    print(sqlText);
                    write_to_file("d:/game_ip_list_config.json",sqlText);


    finally:
        file_object.close();
        print(txtLine);

    print(txtLine);
    print("end");
