# -*- coding: utf-8 -*-

import os;

allGroup = [];


def confirmGroup(name):
    group = "其他";
    if name.find("self") >= 0:
        group = "self";
    elif name.find("CCTV") >= 0 or name.find("cctv") >= 0:
        group = "CCTV";
    elif name.find("卫视") >= 0 or name.find("广州") >= 0 or name.find("上海") >= 0 or name.find("广东") >= 0 or name.find(
            "湖北") >= 0 or name.find("翡翠台") >= 0 or name.find("TVB") >= 0 or name.find("视[HD]") >= 0:
        group = "卫视";
    elif name.find("电视剧") >= 0:
        group = "电视剧";
    elif name.find("电影") >= 0 or name.find("影院") >= 0 or name.find("影视") >= 0:
        group = "电影";
    elif name.find("动漫") >= 0 or name.find("卡通") >= 0 or name.find("卡酷") >= 0 or name.find("动画") >= 0:
        group = "动漫";
    elif name.find("综合") >= 0 or name.find("新闻") >= 0 or name.find("历史") >= 0 or name.find("英语") >= 0 or name.find(
            "地理") >= 0 or name.find("物理") >= 0 or name.find("Discovery") >= 0 or name.find("NEWS") >= 0 or name.find(
        "靖天") >= 0 or name.find("New") >=0:
        group = "综合";
    elif name.find("体育") >= 0:
        group = "体育";
    elif name.find("电竞") >= 0 or name.find("游戏") >= 0:
        group = "电竞";
    elif name.find("纪实") >= 0:
        group = "纪实";
    elif name.find("戏剧") >= 0:
        group = "戏剧"
    elif name.find("超清") >= 0:
        group = "超清"
    elif name.find("旅游") >= 0:
        group = "旅游";
    elif name.find("时尚") >= 0:
        group = "时尚";
    elif name.find("财经") >= 0:
        group = "财经";

    return group;


if __name__ == '__main__':
    f = open('live.txt', encoding="utf-8")
    allLive = [];
    # 未分类
    allOther = "";

    result = "[";
    length = 0;
    for line in f.readlines():  # 逐行读取数据
        line = line.strip()  # 去掉每行头尾空白
        # print(line)
        if line is not None:
            lineArr = line.split(",");
            if len(lineArr) >= 2:
                if lineArr[1] not in allLive:
                    group = confirmGroup(lineArr[0]);
                    url = lineArr[1];
                    if url.find(".flv") < 0 and url.find(".m3u8") < 0:
                        if len(lineArr) >= 3:
                            url = lineArr[1] + lineArr[2];
                    result += "{\"name\":\"" + lineArr[0] + "\",\"url\":\"" + url.strip() + "\",\"image\":\"\",\"group\":\"" + group + "\"},";
                    print(lineArr[0] + " link：" + url.strip() + " group: " + group);
                    length = length + 1;
                    allLive.append(lineArr[1]);
                    if group == "其他":
                        allOther += lineArr[0] + "," + url + "\n";

    result = result[:-1];
    result += "]";
    if os.path.exists("../../../live.json"):
        os.remove("../../../live.json");
    liveJson = open('../../../live.json', 'w', encoding="utf-8");
    liveJson.write(result)

    if os.path.exists("other.txt"):
        os.remove("other.txt");
    otherTxt = open('other.txt', 'w', encoding="utf-8");
    otherTxt.write(allOther)

    print(length);
