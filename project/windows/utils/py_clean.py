# -*- coding: utf-8 -*-

'''
删除文件或者文件夹 2019年8月30日09:58:24
'''

import sys
import os
import shutil

'''
删除文件夹
'''


def del_folder(path):
    if os.path.exists(path):
        for i in os.listdir(path):
            path_file = os.path.join(path, i)  # 取文件绝对路径
            if os.path.exists(path_file):
                if os.path.isfile(path_file):
                    os.remove(path_file);
                    print("remove " + path_file);
                else:
                    shutil.rmtree(path_file)  # 递归删除文件夹
                    print("rmtree " + path_file);
    else:
        print(" not path" + path);


if __name__ == '__main__':
    print("start ")
    try:
        if os.path.exists(sys.argv[1]):
            del_folder(sys.argv[1]);
        else:
            print("您输入的路径不存在!")
    except Exception as e:
        print("program error %s " % e)
    finally:
        print("copy finally print!")
        print("end ")
