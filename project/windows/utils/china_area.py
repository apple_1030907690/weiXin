# -*- coding: utf-8 -*-

'''
导出国家数据
'''

import os
import time
import pymysql

host = '127.0.0.1';

port = 3306;
user = 'root';
passwd = 'root';
db = 'script';
charset = 'UTF8'


# 写入文件
def write_to_file(file_name, txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件" + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读

    'w'：写

    'a'：追加

    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）

    'w+' == w+r（可读可写，文件若不存在就创建）

    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'a+', encoding='utf-8');
    # 2 读写文件
    f.write(str(txt));
    # 3 关闭文件
    f.close();


def name():
    conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db,
                           charset=charset);
    cur = conn.cursor();
    sql = "SELECT short_name FROM `china_area` where pid = 0 ";
    cur.execute(sql);
    # 获取所有记录列表
    results = cur.fetchall();
    for row in results:
        # print(row[0]+",");
        write_to_file("e:/area.txt", "\""+row[0] + "\""+",");


if __name__ == '__main__':
    print(" start  ");

    name();
    print(" end ");
    random_input = input(" 请输入任意字符结束. ");
