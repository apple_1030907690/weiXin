# -*- coding: utf-8 -*-

'''
查找端口
'''

import os

# execute command, and return the output
def execCmd(cmd):
    r = os.popen(cmd)
    text = r.read()
    r.close()
    return text

if __name__ == '__main__':
    print("start");
    port = input("input port   ");
    command = "netstat -ano |findstr \""+port+"\"";
    result = execCmd(command);
    if result:
        print(result);
        result_pid_line = str(result).find("LISTENING");
        result_pid = result[int(result_pid_line):len(result)];
        result_pid = result_pid.replace("LISTENING","").strip();

        print("result_pid "+result_pid)

        command = "tasklist | findstr \""+result_pid+"\"";
        result = execCmd(command);
        print(str(result).split(" ")[0]);

        kill = input("input 1 kill ");

        if "1" == kill:
            command = "taskkill /f /t /im "+str(result).split(" ")[0];
            result = execCmd(command);
            print(str(result));

    print("end");

