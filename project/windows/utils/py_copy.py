# -*- coding: utf-8 -*-

'''
copy文件或者文件夹 2019年8月30日09:58:24
'''

import sys
import os
import shutil

if __name__ == '__main__':
    print("start ")
    try:
        if os.path.exists(sys.argv[1]):
            if os.path.isfile(sys.argv[1]):
                shutil.copy(sys.argv[1], sys.argv[2]);
                print("copy file " + sys.argv[1] + " " + sys.argv[2]);
            elif os.path.isdir(sys.argv[1]):
                shutil.copytree(sys.argv[1], sys.argv[2]);
                print("copy folder" + sys.argv[1] + " " + sys.argv[2]);
        else:
            print("第一个路径有不存在的!")
    except Exception as e:
        print("program error %s " % e)
    finally:
        print("copy finally print!")
        print("end ")
