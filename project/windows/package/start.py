# -*- coding: utf-8 -*-

'''
启动应用
'''


import os

#需要执行的python文件
class PythonFile:
    def __init__(self, name, path,number):
        self.name = name;
        self.path = path;
        self.number = number;

    def getName(self):
        return self.name;

    def getPath(self):
        return self.path;


    def getNumber(self):
        return self.number;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False



if __name__ == '__main__':

    print("start");

    # 排除执行的脚本文件
    exclude_project = ["start.py", "README.md"];
    pythonFileList = [];

    for py_file_name in os.listdir("./"):
        if  py_file_name not in exclude_project:
            py_file_name_arr = py_file_name.split("_");

            #文件的执行顺序
            invoke_number = 0; #默认使用0
            if len(py_file_name_arr) > 1:
                invoke_number = py_file_name_arr[1].strip(".py");
            pythonFile = PythonFile(py_file_name, "./"+py_file_name, int(invoke_number));
            pythonFileList.append(pythonFile);



    #排序 这里是升序  python list对象排序 https://blog.csdn.net/lianshaohua/article/details/80483357
    pythonFileList.sort();
    for py_file_name_item in pythonFileList:
        print("invoke " + py_file_name_item.getName());
        os.system("python " + py_file_name_item.getName());


    print("end ");

    #random_input = input(" 请输入任意字符结束. ");
