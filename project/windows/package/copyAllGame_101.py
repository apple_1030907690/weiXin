# -*- coding: utf-8 -*-

'''
复制应用
'''

import os
import shutil

if __name__ == '__main__':
    print("copyAllGame start");
    basePath = "D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/";
    targetPath = "F:/java_runtime/";

    project_list = ["center-server","db-server","gate-server","hall-server","login-server","game-baccara","game-bullfight","game-cqssc",
                    "game-dragontiger","game-fores","game-fruit-machine","game-gemstorm","game-glodflower","game-gragontiles","game-red-black","game-robtaurus"];

    for item in project_list:
        if False == os.path.exists(targetPath + item):
            os.makedirs(targetPath+item);
        if os.path.exists(targetPath + item + "/res"):
            shutil.rmtree(targetPath + item + "/res");
        shutil.copytree(basePath + project_list[0] + "/target/res", targetPath + item + "/res");
        if os.path.exists(targetPath + item +"/"+item+ "-1.0.0.jar"):
            os.remove(targetPath + item +"/"+item+ "-1.0.0.jar");
        print("copy "+basePath+item+"/target/"+item+"-1.0.0.jar")
        shutil.copy(basePath+item+"/target/"+item+"-1.0.0.jar",targetPath + item +"/" +item+ "-1.0.0.jar");

    libs = basePath + project_list[0] + "/target/lib";

    lib = basePath + project_list[5] + "/target/lib";

    #复制lib和libs
    print("复制lib和libs start ");
    if os.path.exists(targetPath+ project_list[0] + "/libs"):
        shutil.rmtree(targetPath+ project_list[0] + "/libs");
    if os.path.exists(targetPath + project_list[0] + "/lib"):
        shutil.rmtree(targetPath + project_list[0] + "/lib");
    shutil.copytree(libs,targetPath+ project_list[0] + "/libs");
    shutil.copytree(lib, targetPath + project_list[0] + "/lib");

    print("复制lib和libs end ");


    print("copyAllGame end");