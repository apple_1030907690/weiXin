# -*- coding: utf-8 -*-

'''
启动子游戏 2018年11月8日15:53:51
'''

import os
import time

#需要执行的python文件
class PackageFile:
    def __init__(self, workDir,jar, mainClass,port,path,logs,number):
        #工作路径
        self.workDir = workDir;
        #jar
        self.jar = jar;
        #main class
        self.mainClass = mainClass;
        #端口
        self.port = port;
        #地址
        self.path = path;
        #日志路径
        self.logs = logs;
        #启动顺序
        self.number = number;


    def getWorkDir(self):
        return self.workDir;

    def getNumber(self):
        return self.number;

    def getJar(self):
        return self.jar;

    def getMainClass(self):
        return self.mainClass;

    def getPort(self):
        return self.port;

    def getPath(self):
        return self.path;

    def getLogs(self):
        return self.logs;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False


if __name__ == '__main__':
    print(" startGameServer start  ");
    #start cmd /k "run_game-provider.bat"
    '''
    basePath = "D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/center-server/target/";
    os.chdir(basePath);
    jar = basePath+"lib/*"+";"+basePath+"center-server-1.0.0.jar ";
    main_class = "com.lyh.game.center.start.ServerStart ";
    port = " 9100 ";
    logs = basePath+"logs";
    #打开cmd窗口
    os.system("start cmd /k  java -cp  "+jar + main_class + port + logs  );
    
    '''



    basePath = "F:/java_runtime/";

    #这是启动的游戏列表
    project_list = [];
    server = PackageFile(basePath+"game-baccara",basePath +"center-server/lib/*"+";"+ basePath+"game-baccara/game-baccara-1.0.0.jar","com.lyh.game.baccara.start.ServerStart","2103",".",basePath+"game-baccara/logs",1);
    project_list.append(server);

    server = PackageFile(basePath + "game-bullfight",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-bullfight/game-bullfight-1.0.0.jar",
                         "com.lyh.game.bullfight.start.ServerStart", "2105", ".", basePath + "game-baccara/logs", 2);
    project_list.append(server);

    server = PackageFile(basePath + "game-cqssc",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-cqssc/game-cqssc-1.0.0.jar",
                         "com.lyh.game.cqssc.start.ServerStart", "2114", ".", basePath + "game-baccara/logs", 3);
    project_list.append(server);



    server = PackageFile(basePath + "game-dragontiger",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-dragontiger/game-dragontiger-1.0.0.jar",
                         "com.lyh.game.dragontiger.start.ServerStart", "2108", ".", basePath + "game-dragontiger/logs",
                         5);
    project_list.append(server);


    server = PackageFile(basePath + "game-fores",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-fores/game-fores-1.0.0.jar",
                         "com.lyh.game.fores.start.ServerStart", "2106", ".", basePath + "game-fores/logs",
                         6);
    project_list.append(server);

    server = PackageFile(basePath + "game-fruit-machine",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-fruit-machine/game-fruit-machine-1.0.0.jar",
                         "com.lyh.game.fruitmachine.start.ServerStart", "2107", ".", basePath + "game-fruit-machine/logs",
                         6);
    project_list.append(server);

    server = PackageFile(basePath + "game-gemstorm",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-gemstorm/game-gemstorm-1.0.0.jar",
                         "com.lyh.game.gemstorm.start.ServerStart", "2113", ".",
                         basePath + "game-fruit-machine/logs",
                         7);
    project_list.append(server);

    server = PackageFile(basePath + "game-glodflower",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-glodflower/game-glodflower-1.0.0.jar",
                         "com.lyh.game.glodflower.start.ServerStart", "2101", ".",
                         basePath + "game-glodflower/logs",
                         8);
    project_list.append(server);

    server = PackageFile(basePath + "game-gragontiles",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-gragontiles/game-gragontiles-1.0.0.jar",
                         "com.lyh.game.gragontiles.start.ServerStart", "2111", ".",
                         basePath + "game-gragontiles/logs",
                        9);
    project_list.append(server);

    server = PackageFile(basePath + "game-red-black",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-red-black/game-red-black-1.0.0.jar",
                         "com.lyh.game.redblack.start.ServerStart", "2102", ".",
                         basePath + "game-red-black/logs",
                         10);
    project_list.append(server);


    server = PackageFile(basePath + "game-robtaurus",
                         basePath + "center-server/lib/*" + ";" + basePath + "game-robtaurus/game-robtaurus-1.0.0.jar",
                         "com.lyh.game.robtaurus.start.ServerStart", "2112", ".",
                         basePath + "game-robtaurus/logs",
                         11);
    project_list.append(server);


    project_list.sort();

    for item in project_list:
        print("正在启动 " + item.getJar());
        os.chdir(item.getWorkDir());
        #print("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() +" "+ item.getLogs());
        os.system("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() + " " + item.getLogs() );
        #暂停几秒
        time.sleep(5);
    print(" startGameServer end ");