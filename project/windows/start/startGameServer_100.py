# -*- coding: utf-8 -*-

'''
启动应用 2018年11月8日15:53:51
'''

import os
import time

#需要执行的python文件
class PackageFile:
    def __init__(self, workDir,jar, mainClass,port,path,logs,number):
        # 工作路径
        self.workDir = workDir;
        # jar
        self.jar = jar;
        # main class
        self.mainClass = mainClass;
        # 端口
        self.port = port;
        # 地址
        self.path = path;
        # 日志路径
        self.logs = logs;
        # 启动顺序
        self.number = number;


    def getWorkDir(self):
        return self.workDir;

    def getNumber(self):
        return self.number;

    def getJar(self):
        return self.jar;

    def getMainClass(self):
        return self.mainClass;

    def getPort(self):
        return self.port;

    def getPath(self):
        return self.path;

    def getLogs(self):
        return self.logs;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False


if __name__ == '__main__':
    print(" startGameServer start  ");
    #start cmd /k "run_game-provider.bat"
    '''
    basePath = "D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/center-server/target/";
    os.chdir(basePath);
    jar = basePath+"lib/*"+";"+basePath+"center-server-1.0.0.jar ";
    main_class = "com.lyh.game.center.start.ServerStart ";
    port = " 9100 ";
    logs = basePath+"logs";
    #打开cmd窗口
    os.system("start cmd /k  java -cp  "+jar + main_class + port + logs  );
    
    '''



    basePath = "F:/java_runtime/";

    #启动的游戏列表
    project_list = [];
    server = PackageFile(basePath+"center-server",basePath +"center-server/libs/*"+";"+ basePath+"center-server/center-server-1.0.0.jar","com.lyh.game.center.start.ServerStart","9100",".",basePath+"center-server/logs",1);
    project_list.append(server);
    server = PackageFile(basePath + "db-server",basePath + "center-server/libs/*" + ";" + basePath + "db-server/db-server-1.0.0.jar","com.lyh.game.db.start.ServerStart", "8100",".", basePath + "db-server/logs", 2);
    project_list.append(server);

    server = PackageFile(basePath + "gate-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "gate-server/gate-server-1.0.0.jar",
                         "com.lyh.game.gate.start.ServerStart", "1100", ".", basePath + "gate-server/logs", 3);
    project_list.append(server);

    server = PackageFile(basePath + "hall-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "hall-server/hall-server-1.0.0.jar",
                         "com.lyh.game.world.start.ServerStart", "3100", ".", basePath + "hall-server/logs", 4);
    project_list.append(server);

    server = PackageFile(basePath + "login-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "login-server/login-server-1.0.0.jar",
                         "com.lyh.game.login.start.ServerStart", "4100", ".", basePath + "login-server/logs", 5);
    project_list.append(server);

    project_list.sort();

    for item in project_list:
        print("正在启动 " + item.getJar());
        os.chdir(item.getWorkDir());
        #print("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() +" "+ item.getLogs());
        os.system("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() + " " + item.getLogs() );
        #暂停几秒
        time.sleep(5);
    print(" startGameServer end ");