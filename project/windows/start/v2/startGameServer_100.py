# -*- coding: utf-8 -*-

'''
启动应用 2018年11月8日15:53:51
'''

import os
import time
import pymysql


# 执行的应用程序
class ApplicationRunTime:
    def __init__(self, id, name, workDir, status, sort, sleep, command, remarks, date):
        self.id = id;
        # 名称
        self.name = name;
        # 工作路径
        self.workDir = workDir;
        # status 1 启用  0 禁用
        self.status = status;
        # sort  排序 小到大
        self.sort = sort;
        # 执行后睡眠时间
        self.sleep = sleep;
        # 执行命令
        self.command = command;
        # 备注
        self.remarks = remarks;
        # 时间
        self.date = date;

    def getId(self):
        return self.id;

    def getWorkDir(self):
        return self.workDir;

    def getName(self):
        return self.name;

    def getStatus(self):
        return self.status;

    def getSort(self):
        return self.sort;

    def getSleep(self):
        return self.sleep;

    def getCommand(self):
        return self.command;

    def getRemarks(self):
        return self.remarks;

    def getDate(self):
        return self.date;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False


'''
#需要执行的python文件
class PackageFile:
    def __init__(self, workDir,jar, mainClass,port,path,logs,number):
        # 工作路径
        self.workDir = workDir;
        # jar
        self.jar = jar;
        # main class
        self.mainClass = mainClass;
        # 端口
        self.port = port;
        # 地址
        self.path = path;
        # 日志路径
        self.logs = logs;
        # 启动顺序
        self.number = number;



    def getWorkDir(self):
        return self.workDir;

    def getNumber(self):
        return self.number;

    def getJar(self):
        return self.jar;

    def getMainClass(self):
        return self.mainClass;

    def getPort(self):
        return self.port;

    def getPath(self):
        return self.path;

    def getLogs(self):
        return self.logs;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False

'''

host = '127.0.0.1';

port = 3306;
user = 'root';
passwd = 'root';
db='script';
charset = 'UTF8'

def getConfig(key):
    conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db,
                           charset=charset);
    cur = conn.cursor();
    sql = "SELECT c.id,c.key,c.value,c.remarks FROM config as c where 1= 1 and c.key='" + key + "'";
    cur.execute(sql);
    # 获取所有记录列表
    results = cur.fetchall();

    result = {};
    for row in results:
        result["id"] = row[0];
        result["key"] = row[1];
        result["value"] = row[2];
    cur.close();
    conn.close();
    return result;



def getApplicationList():
    conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db,
                           charset=charset);
    cur = conn.cursor();
    sql = "SELECT id,name,work_dir ,status, sort,sleep,command,remarks,date FROM  application where 1=1 and status='1'" + "  order by  sort asc";
    cur.execute(sql);
    # 获取所有记录列表
    results = cur.fetchall();

    project_list = [];
    for row in results:
        application = ApplicationRunTime(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]);
        project_list.append(application);
    cur.close();
    conn.close();
    return project_list;

if __name__ == '__main__':
    print(" startGameServer start  ");
    # start cmd /k "run_game-provider.bat"
    '''
    basePath = "D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/center-server/target/";
    os.chdir(basePath);
    jar = basePath+"lib/*"+";"+basePath+"center-server-1.0.0.jar ";
    main_class = "com.lyh.game.center.start.ServerStart ";
    port = " 9100 ";
    logs = basePath+"logs";
    #打开cmd窗口
    os.system("start cmd /k  java -cp  "+jar + main_class + port + logs  );
    
    '''

    '''
    basePath = "F:/java_runtime/";

    #启动的游戏列表
    project_list = [];
    server = PackageFile(basePath+"center-server",basePath +"center-server/libs/*"+";"+ basePath+"center-server/center-server-1.0.0.jar","com.lyh.game.center.start.ServerStart","9100",".",basePath+"center-server/logs",1);
    project_list.append(server);
    server = PackageFile(basePath + "db-server",basePath + "center-server/libs/*" + ";" + basePath + "db-server/db-server-1.0.0.jar","com.lyh.game.db.start.ServerStart", "8100",".", basePath + "db-server/logs", 2);
    project_list.append(server);

    server = PackageFile(basePath + "gate-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "gate-server/gate-server-1.0.0.jar",
                         "com.lyh.game.gate.start.ServerStart", "1100", ".", basePath + "gate-server/logs", 3);
    project_list.append(server);

    server = PackageFile(basePath + "hall-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "hall-server/hall-server-1.0.0.jar",
                         "com.lyh.game.world.start.ServerStart", "3100", ".", basePath + "hall-server/logs", 4);
    project_list.append(server);

    server = PackageFile(basePath + "login-server",
                         basePath + "center-server/libs/*" + ";" + basePath + "login-server/login-server-1.0.0.jar",
                         "com.lyh.game.login.start.ServerStart", "4100", ".", basePath + "login-server/logs", 5);
    project_list.append(server);

    project_list.sort();

    for item in project_list:
        print("正在启动 " + item.getJar());
        os.chdir(item.getWorkDir());
        #print("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() +" "+ item.getLogs());
        os.system("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() + " " + item.getLogs() );
        #暂停几秒
        time.sleep(5);
    '''



    basePath = getConfig("base_path")["value"];

    project_list = getApplicationList();

    for item in project_list:
        print("正在启动 " + item.getName());
        os.chdir(item.getWorkDir());
        # print("start cmd /k  java -cp  "+ item.getJar() + " "+ item.getMainClass() + " "+ item.getPort()  +" " + item.getPath() +" "+ item.getLogs());
        os.system(item.getCommand());
        # 暂停几秒
        time.sleep(item.getSleep());
    print(" startGameServer end ");



