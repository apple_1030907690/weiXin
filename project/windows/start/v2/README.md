启动程序第二版  使用数据库
start.py启动是启动全部


cmd命令:
   复制文件夹
        /S 复制目录和子目录，不包括空目录。
        /E 复制目录和子目录，包括空目录。与 /S /E 相同。可以用来修改 /T。

        xcopy  D:\work\newFramework\east-meeting-chess-server\trunk\east-meeting-chess-server\log-server\target  F:\java_runtime\log-server  /e /s

        xcopy总是询问是文件名还是目录名,可以这样通过管道来做:
           echo d | xcopy  D:\work\newFramework\east-meeting-chess-server\trunk\east-meeting-chess-server\log-server\target  F:\java_runtime\log-server  /e /s

    复制单个文件:
         copy  D:\work\newFramework\east-meeting-chess-server\trunk\east-meeting-chess-server\log-server\target\log-server-1.0.0.jar  F:\java_runtime\log-server

    删除单个文件:
        del F:\java_runtime\log-server\log-server-1.0.0.jar
        支持通配符如:del /F /S /Q F:\java_runtime\log-server\*.txt   强制删除F:\java_runtime\log-server下的所以txt文件
    删除文件夹:
      /s 是代表删除所有子目录跟其中的档案。
      /q 是不要它在删除档案或目录时，不再问我 Yes or No 的动作。
      rmdir /s /q   F:\java_runtime\log-server







建表语句:
    CREATE DATABASE `script`;

    CREATE TABLE `config` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
      `key` varchar(255) NOT NULL COMMENT '键',
      `value` varchar(255) NOT NULL COMMENT '值',
      `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
      `date` datetime NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';

    INSERT INTO `script`.`config` (`id`, `key`, `value`, `remarks`, `date`) VALUES ('1', 'base_path', 'F:/java_runtime/', '运行根路径', '2018-11-12 22:36:40');

    CREATE TABLE `application` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL COMMENT '应用名称',
      `work_dir` text NOT NULL COMMENT '工作路径',
      `status` int(1) NOT NULL COMMENT '状态 1 是开启 0 是禁用',
      `sort` int(11) NOT NULL DEFAULT '0' COMMENT '启动顺序 小到大',
      `sleep` int(11) NOT NULL DEFAULT '0' COMMENT '启动后睡眠几秒',
      `command` text NOT NULL COMMENT '启动命令',
      `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
      `date` datetime NOT NULL COMMENT '时间',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用表';

    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (1, '中心服', 'D:/java_runtime/center-server', 1, 1, 5, 'start cmd /k  java -cp  D:/java_runtime/center-server/libs/*;D:/java_runtime/center-server/center-server-1.0.0.jar com.lyh.game.center.start.ServerStart 9100 . D:/java_runtime/center-server/logs', '中心服', '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (2, 'db服务', 'D:/java_runtime/db-server', 1, 2, 5, 'start cmd /k  java -cp  D:/java_runtime/center-server/libs/*;D:/java_runtime/db-server/db-server-1.0.0.jar com.lyh.game.db.start.ServerStart 8100 . D:/java_runtime/db-server/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (3, '网关服务', 'D:/java_runtime/gate-server', 1, 3, 5, 'start cmd /k  java -cp  D:/java_runtime/center-server/libs/*;D:/java_runtime/gate-server/gate-server-1.0.0.jar com.lyh.game.gate.start.ServerStart 1100 . D:/java_runtime/gate-server/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (4, '大厅服务', 'D:/java_runtime/hall-server', 1, 4, 5, 'start cmd /k  java -cp  D:/java_runtime/center-server/libs/*;D:/java_runtime/hall-server/hall-server-1.0.0.jar com.lyh.game.world.start.ServerStart 3100 . D:/java_runtime/hall-server/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (5, '登录服务', 'D:/java_runtime/login-server', 1, 5, 5, 'start cmd /k  java -cp  D:/java_runtime/center-server/libs/*;D:/java_runtime/login-server/login-server-1.0.0.jar com.lyh.game.login.start.ServerStart 4100 . D:/java_runtime/login-server/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (6, 'baccara', 'D:/java_runtime/game-baccara', 1, 6, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-baccara/game-baccara-1.0.0.jar com.lyh.game.baccara.start.ServerStart 2103 . D:/java_runtime/game-baccara/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (7, 'bullfight', 'D:/java_runtime/game-bullfight', 1, 7, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-bullfight/game-bullfight-1.0.0.jar com.lyh.game.bullfight.start.ServerStart 2105 . D:/java_runtime/game-bullfight/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (8, 'cqssc', 'D:/java_runtime/game-cqssc', 1, 8, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-cqssc/game-cqssc-1.0.0.jar com.lyh.game.cqssc.start.ServerStart 2114 . D:/java_runtime/game-cqssc/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (9, 'dragontiger', 'D:/java_runtime/game-dragontiger', 1, 9, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-dragontiger/game-dragontiger-1.0.0.jar com.lyh.game.dragontiger.start.ServerStart 2108 . D:/java_runtime/game-dragontiger/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (10, 'fores', 'D:/java_runtime/game-fores', 1, 10, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-fores/game-fores-1.0.0.jar com.lyh.game.fores.start.ServerStart 2106 . D:/java_runtime/game-fores/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (11, 'fruit-machine', 'D:/java_runtime/game-fruit-machine', 1, 11, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-fruit-machine/game-fruit-machine-1.0.0.jar com.lyh.game.fruitmachine.start.ServerStart 2107 . D:/java_runtime/game-fruit-machine/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (12, 'gemstorm', 'D:/java_runtime/game-gemstorm', 1, 12, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-gemstorm/game-gemstorm-1.0.0.jar com.lyh.game.gemstorm.start.ServerStart 2113 . D:/java_runtime/game-gemstorm/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (13, 'glodflower', 'D:/java_runtime/game-glodflower', 1, 13, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-glodflower/game-glodflower-1.0.0.jar com.lyh.game.glodflower.start.ServerStart 2101 . D:/java_runtime/game-glodflower/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (14, 'gragontiles', 'D:/java_runtime/game-gragontiles', 1, 14, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-gragontiles/game-gragontiles-1.0.0.jar com.lyh.game.gragontiles.start.ServerStart 2111 . D:/java_runtime/game-gragontiles/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (15, 'red-black', 'D:/java_runtime/game-red-black', 1, 15, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-red-black/game-red-black-1.0.0.jar com.lyh.game.redblack.start.ServerStart 2102 . D:/java_runtime/game-red-black/logs', NULL, '2018-11-12 22:55:13');
    INSERT INTO `script`.`application`(`id`, `name`, `work_dir`, `status`, `sort`, `sleep`, `command`, `remarks`, `date`) VALUES (16, 'robtaurus', 'D:/java_runtime/game-robtaurus', 1, 16, 1, 'start cmd /k  java -cp  D:/java_runtime/center-server/lib/*;D:/java_runtime/game-robtaurus/game-robtaurus-1.0.0.jar com.lyh.game.robtaurus.start.ServerStart 2112 . D:/java_runtime/game-robtaurus/logs', NULL, '2018-11-12 22:55:13');












