# -*- coding: utf-8 -*-

'''
2019年1月15日18:42:25
删除指定文件
'''

import os;

iml = ".iml";

delete_file_list = [];

def for_folder(path):
    if os.path.exists(path):
        for i in os.listdir(path):
            path_file = os.path.join(path, i)  # 取文件绝对路径
            if os.path.exists(path_file):
                if os.path.isfile(path_file):
                   #print("file " + path_file);
                    suffix = os.path.splitext(path_file)[-1];
                    if iml == suffix:
                        #print(path_file);
                        delete_file_list.append(path_file);
                else:
                    for_folder(os.path.join(path, i));
                    # 文件夹
                    #print("文件夹 " + path_file);
    else:
        print(" not path" + path);


if __name__ == '__main__':
    print("start");

    random_input = input(" 请输入要删除的文件后缀名,默认是.iml. ");
    if random_input != None and "" != random_input:
        iml = random_input;

    for_folder(os.getcwd());


    print("删除当前目录及子目录下文件后缀为 " + iml +"的文件");


    for item in delete_file_list:
        print("remove "+ item);
        os.remove(item);

    print("end");
    random_input = input(" 请输入任意字符结束. ");
