# -*- coding: utf-8 -*-

'''
adb 脚本的测试 刷宝脚本
zhouzhongqing
2019年6月6日12:58:06
'''

'''
输入 0-9 
adb shell input keyevent 7至16
返回桌面
adb shell input keyevent 3
返回键 
adb shell input keyevent 4

点击事件
adb -s <android手机地址> shell input tap 400 400

向下滑动  adb shell input swipe <X1> <Y1> <X2> <Y2>
adb -s 127.0.0.1:62025 shell input swipe 100 500 100 450


'''
import os
import time
import random
import threading

adb_bin_prefix = "F:/software/platform-tools/";
os.chdir(adb_bin_prefix);
adb_command = "adb.exe ";

'''
执行命令并且得到输出结果
'''


def execCmd(cmd):
    r = os.popen(cmd)
    text = r.read()
    r.close()
    return text


'''
解析地址
'''


def parse_address():
    address_array = [];
    devices = execCmd(adb_command + " devices");
    devices_array = str(devices).split("\n");

    for index in range(len(devices_array)):
        if index > 0 and devices_array[index] != '':
            # 最终地址
            final_address = devices_array[index].strip("\tdevice");
            address_array.append(final_address);
    return address_array;



#线程
class myThread(threading.Thread):  # 继承父类threading.Thread
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):  # 把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        item = self.name;
        #os.system(adb_command_open_app % item);
        #time.sleep(10);
        while True:
            loopCount = random.randint(10, 20);
            #for 循环睡眠
            for index in range(loopCount):
                # 在这里睡眠
                time.sleep(1);

            print( item +" enter details " + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()));
            swipe_random = random.randint(350, 450);
            os.system(adb_command_swipe_random % (item, str(swipe_random)));

            #尝试退出
            # 返回
            os.system(adb_command_back % item);
            # 确保退出再点一次退出
            time.sleep(2);
            os.system(adb_command_back % item);
            # 等2秒再强制退出
            time.sleep(2);
            os.system(adb_command_back % item);

if __name__ == '__main__':
    print("start");

    # 点击
    adb_command_click = adb_command + " -s %s shell input tap 200 300";
    # 向下滑动
    adb_command_swipe = adb_command + " -s %s shell input swipe 100 700 100 400";

    # 向下滑动随机
    adb_command_swipe_random = adb_command + " -s %s shell input swipe 100 700 100 %s";

    # 主要是打开app
    adb_command_open_app = adb_command + " -s %s shell input tap 500 850";

    # 返回
    adb_command_back = adb_command + " -s %s shell input keyevent 4";

    address_array = parse_address();
    print(address_array);
    threads = [];
    for item in address_array:
        new_thread = myThread(item);
        new_thread.start();
        threads.append(new_thread);

        # 等待所有线程完成
    for t in threads:
        t.join();

    print("end");
