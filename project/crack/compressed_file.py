# -*- coding: utf-8 -*-
'''
2019年7月26日11:13:24
压缩文件破解
'''

import subprocess
from tqdm import tqdm
from itertools import product

# 密码生成器
def psgen(x=4):
    iter = ['1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
           # 'abcdefghijklmnopqrstuvwxyz',
            #'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            ]
    for r in iter:
        for repeat in range(1, x + 1):
            for ps in product(r, repeat=repeat):
                yield ''.join(ps)


if __name__ == '__main__':
    '''
    win_rar_exe_prefix = "C:/Program Files/WinRAR/";
    # WinRAR.exe 带图形化界面
    # UnRAR.exe 不是图形化界面
    # "C:/Program Files/WinRAR/WinRAR.exe" e F:/360极速浏览器下载/netty/Netty实战.rar  F:/360极速浏览器下载/netty
    command = win_rar_exe_prefix + "UnRAR.exe -p11234 e F:/360极速浏览器下载/netty/Netty实战.rar  F:/360极速浏览器下载/netty";
    child = subprocess.call(command);
    print(child)
    if child == 0:
        print("解压成功")
        
    '''

    exe_7z_prefix = "F:/software/7-Zip/";
    file_path = "F:/360极速浏览器下载/netty/Netty实战.zip";

    # 解压
    # command = exe_7z_prefix + "7z.exe -p1234 x F:/360极速浏览器下载/netty/Netty实战.zip -oF:/360极速浏览器下载/netty";

    flag = 0;
    for index in range(1, 10):
        if flag > 0:
            break;
        print("bit " + str(index))
        for ps in tqdm(psgen(index)):
            if flag > 0:
                break;
            #print(ps)
            password = ps;
            # 测试解压是否成功

            command = exe_7z_prefix + "7z.exe -p" + password + " t " + file_path;
            child = subprocess.call(command);
            if child == 0:
                flag = 1;
                print("解压成功 password:" + password );
                break;