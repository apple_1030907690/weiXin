# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import JsonResponse

# mysql
import pymysql
# http
import requests
# 调用系统shell命令
import os
from subprocess import call
# 线程
import _thread
import time
import threading

import hashlib
import json
from django.utils.encoding import smart_str

from django.views.decorators.csrf import csrf_exempt

from django.template.loader import render_to_string
from django.views.decorators.csrf import ensure_csrf_cookie
from lxml import etree
import urllib3
from django.core.cache import cache
import sys
# 第一种导入方式
# sys.path.append(r'D:\PycharmProjects\weiXin\project\utils')
# 这里使用的是第二种  在Python安装目录下的\Lib\site-packages文件夹中建立一个.pth文件，内容为自己写的库路径 D:\PycharmProjects\weiXin\project\utils
from Utils import Utils;


def home(request):
    return render(request, "views/admin/home.html");