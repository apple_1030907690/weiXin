# -*- coding: utf-8 -*-

'''
测试服上传热更的代码
'''

import paramiko
import os
import time

host = "47.105.106.222";
user_name = "root";
password = "";


def createConnection():
    ssh = paramiko.SSHClient()
    # 自动认证
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, 22, user_name, password);
    return ssh;


if __name__ == '__main__':
    print("start");
    ssh = createConnection();
    base_path = "C:/Users/Administrator/Desktop/";
    UPDATES_ZIP = "updates.zip";
    LBDWC_HOT_ZIP = "lbdwc_hot.zip";
    upload_file_list = [UPDATES_ZIP, LBDWC_HOT_ZIP];
    __MACOSX = "__MACOSX";
    upload_base_path = "/usr/local/nginx/html/GameVersion/";
    GAME_VERSION_NEW = "game_version_new.txt";

    upload_file_list_file = [];
    #确定要上传的文件
    for file_item in os.listdir(base_path):
        if file_item in upload_file_list:
            print("scan " + file_item);
            upload_file_list_file.append(file_item)


    #删除、上传文件
    # 实例化一个 sftp对象,指定连接的通道
    sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
    for file_item in upload_file_list_file:
        print("rm -rf " + file_item);
        stdin, stdout, stderr = ssh.exec_command("rm -rf " + upload_base_path + file_item);
        print(stdout.readlines());
        stdin, stdout, stderr = ssh.exec_command("rm -rf " + upload_base_path + str(file_item).strip(".zip"));
        print(stdout.readlines());
        print("upload "+ file_item);
        # 发送文件
        sftp.put(localpath=base_path + file_item, remotepath=os.path.join(upload_base_path, file_item));
        
    
    
    sftp.close()

    #删除__MACOSX
    print(" rm -rf  "+ __MACOSX)
    stdin, stdout, stderr = ssh.exec_command("rm -rf " + upload_base_path + __MACOSX);
    print(stdout.readlines());

    #解压文件 替换热更文件
    for file_item in upload_file_list_file:
        print("unzip "+file_item);
        stdin, stdout, stderr = ssh.exec_command("cd " + upload_base_path +" && unzip "+file_item );
        print(stdout.readlines());
        if file_item == UPDATES_ZIP:
            print("cd " + upload_base_path + str(file_item).strip(".zip") + " && \cp -R "+upload_base_path + str(file_item).strip(".zip")+"/*  " + upload_base_path)
            stdin, stdout, stderr = ssh.exec_command("cd " + upload_base_path + str(file_item).strip(".zip") + " && \cp -R "+upload_base_path + str(file_item).strip(".zip")+"/*  " + upload_base_path);
            print(stdout.readlines());




    #查看和替换版本文件
    print(" cat  "+ upload_base_path + GAME_VERSION_NEW);
    stdin, stdout, stderr = ssh.exec_command(" cat  "+ upload_base_path + GAME_VERSION_NEW);
    print(stdout.readlines());

    print(" replace hotVersion ");
    random_input = input(" 请输入替换语句. 如 1.0.8/1.0.9  ");
    print("sed -i  's/"+random_input+"/g' "+ upload_base_path + GAME_VERSION_NEW)
    stdin, stdout, stderr = ssh.exec_command("sed -i 's/"+random_input+"/g' "+ upload_base_path + GAME_VERSION_NEW);
    print(stdout.readlines());

    print(" cat  " + upload_base_path + GAME_VERSION_NEW);
    stdin, stdout, stderr = ssh.exec_command(" cat  " + upload_base_path + GAME_VERSION_NEW);
    print(stdout.readlines());

    ssh.close();
    print("end");
    random_input = input(" 请输入任意字符结束. ");
