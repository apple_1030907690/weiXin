# -*- coding: utf-8 -*-

'''
zhouzhongqing
2018年4月10日16:34:05
把target下的包复制过来 - 复制文件 测试
'''

import os
import shutil

if __name__ == '__main__':
    print("start");

    '''
      if True == os.path.exists(toCopyPath+"sf.txt"):
          print("exits")
          os.remove(toCopyPath + "sf.txt");
      else :
          print(" no exits");

      '''

    basePath = "D:/work/east-meeting-server/trunk/eastMeetingJava2/";
    toCopyPath = "C:/Users/Administrator/Desktop/package/";

    #war包list
    packageList = ["game-consumer", "game-channel"];

    #jar包list
    packageJarList = ["game-landlords","game-provider","game-red-black","game-gold-flower","game-baccara"];






    # 删除之前的包
    for package in packageList:
        baseWarPath = toCopyPath + package + "/" + "ROOT.war";
        if True == os.path.exists(baseWarPath):
            os.remove(baseWarPath);
            print("remove ",baseWarPath)

    for package in packageJarList:
        baseJarPath = toCopyPath  + package +"/" + package   + "-1.0-SNAPSHOT-bin.zip";
        if True == os.path.exists(baseJarPath):
            os.remove(baseJarPath);
            print("remove " + baseJarPath);


    #重新copy

    target = "/target/";

    for package in packageList:
        baseWarPath = basePath + package + target + "ROOT.war";
        if True == os.path.exists(baseWarPath):
            shutil.copy(baseWarPath, toCopyPath + package + "/" + "ROOT.war");
            print("doWork copy " + baseWarPath);

    for package in packageJarList:
        baseJarPath = basePath + package + target + package+"-1.0-SNAPSHOT-bin.zip";
        if True == os.path.exists(baseJarPath):
            shutil.copy(baseJarPath, toCopyPath + package + "/" + package + "-1.0-SNAPSHOT-bin.zip");
            print("doWork copy " + baseJarPath);




    #热更包

    lbdwcHotPath = "C:/Users/Administrator/Desktop/";
    targetHotUpdate = "lbdwc_hot";
    lbdwcHotName = "lbdwc_hot2.zip";
    baseHotPath = lbdwcHotPath +targetHotUpdate + "/" + lbdwcHotName;

    #删除
    if True == os.path.exists(toCopyPath + targetHotUpdate + "/" + lbdwcHotName):
        os.remove(toCopyPath + targetHotUpdate + "/" + lbdwcHotName);

    if True == os.path.exists(baseHotPath):
         shutil.copy(baseHotPath, toCopyPath + targetHotUpdate + "/" + lbdwcHotName);
         print("doWork copy " + baseHotPath);



    print("end");
