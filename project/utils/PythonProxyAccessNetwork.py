# -*- coding: utf-8 -*-

# python通过代理访问网络

import os
import sys
import urllib.request
import requests
# 代理的地址帐号密码等
PROXY_INFO = {
    'user': '',
    'pass': '',
    'host': '127.0.0.1',
    'port': 1080
}

#使用urllib用代理访问
def load_url_urllib_proxy(url):
    proxy_support = urllib.request.ProxyHandler({'http': \
                                             'http://%(user)s:%(pass)s@%(host)s:%(port)d' % PROXY_INFO})
    opener = urllib.request.build_opener(proxy_support, urllib.request.HTTPHandler)
    urllib.request.install_opener(opener)
    # 头信息
    user_agent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;";
    headers = {'User-Agent': user_agent};
    req = urllib.request.Request(url, headers=headers);

    response = urllib.request.urlopen(req);
    return str(response.read(),'utf-8') ;

#使用requests用代理访问
def load_url_requests_proxy(url):
    proxies = {"http": "http://127.0.0.1:1080", "https": "https://127.0.0.1:1080"};
    response = requests.get(url, proxies=proxies);
    return response;

def load_page(url):
    '''''
    发送URL请求
    '''
    user_agent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;";
    headers = {'User-Agent': user_agent};
    req = urllib.request.Request(url, headers=headers);
    response = urllib.request.urlopen(req);
    html = str(response.read(), 'utf-8')
    # html = response.read();  这代码出现一种byte字节　　详见　http://www.zhihu.com/question/27062410
    print('loading...baidu');
    return html;


#写入文件
def write_to_file(file_name,txt):
    '''''
        讲txt文本存入到file_name文件中
    '''
    print("正在存储文件"+str(file_name));
    #1 打开文件
    #w 如果没有这个文件将创建这个文件
    f = open(file_name,'w',encoding='utf-8');
    #2 读写文件
    f.write(str(txt));
    #3 关闭文件
    f.close();

if __name__ == '__main__':
   # html = load_url_urllib_proxy("https://www.google.com.hk/");
    #print(html);
    html = load_url_requests_proxy("https://whoer.net/zh");
    write_to_file("C:/Users/Administrator/Desktop/test.html",html.text);
    print(html.text );
    print(html.content );

