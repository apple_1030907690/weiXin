# -*- coding: utf-8 -*-

'''

zhouzhongqing
2018年5月2日16:17:13
读取properties文件

'''

import re
import os
import tempfile


class Properties:

    def __init__(self, file_name):
        self.file_name = file_name
        self.properties = {}
        try:
            fopen = open(self.file_name, 'r')
            for line in fopen:
                line = line.strip()
                if line.find('=') > 0 and not line.startswith('#'):
                    strs = line.split('=')
                    self.properties[strs[0].strip()] = strs[1].strip()
        except Exception:
            #raise Exception
            print("read file exception ...");
        else:
            fopen.close();

    def has_key(self, key):
        return key in self.properties

    def get(self, key, default_value=''):
        if key in self.properties:
            return self.properties[key]
        return default_value

    def put(self, key, value):
        self.properties[key] = value
        replace_property(self.file_name, key + '=.*', key + '=' + value, True)


def parse(file_name):
    return Properties(file_name)


def replace_property(file_name, from_regex, to_str, append_on_not_exists=True):
    tmpfile = tempfile.TemporaryFile()

    if os.path.exists(file_name):
        r_open = open(file_name, 'r')
        pattern = re.compile(r'' + from_regex)
        found = None
        for line in r_open:
            if pattern.search(line) and not line.strip().startswith('#'):
                found = True
                line = re.sub(from_regex, to_str, line)
            tmpfile.write(bytes(line, encoding="utf8"))
        if not found and append_on_not_exists:
            tmpfile.write( bytes('\n' + to_str ,encoding="utf-8"))
        r_open.close()
        tmpfile.seek(0)

        content = tmpfile.read()

        if os.path.exists(file_name):
            os.remove(file_name)

        w_open = open(file_name, 'w')
        w_open.write(str(content,encoding="utf-8"))
        w_open.close()

        tmpfile.close()
    else:
        print("file %s not found" % file_name);


if __name__ == '__main__':
    file_path = './test.properties'
    property = Properties(file_path); # 读取文件
    print(property.get("pwd")); # 根据key读取value
    property.put("abc","f"); # 修改/添加key=value
    print(property.get("abc"));
    print("props.has_key('pwd')=" + str(property.has_key('pwd')))  # 判断是否包含该key

