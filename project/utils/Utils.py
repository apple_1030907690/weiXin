# -*- coding: utf-8 -*-
import json
import requests


class Utils:
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_inst'):
            cls._inst = super(Utils, cls).__new__(cls, *args, **kwargs)
        return cls._inst

    '''
    获取access_token
    '''
    CGI_BIN_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

    '''
    创建菜单
    '''
    MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";

    APPID = "wxa7b18b3ae7780246";

    APPSECRET = "14f7fa9f10ab56acad64bbbda491f711";


    HOST = "localhost";
    PORT = 3306;
    USER = 'root';
    PASSWD = 'root';
    DB = 'video';
    CHARSET = 'UTF8'


    # def __init__(self):


if __name__ == '__main__':
    a = Utils();
    b = Utils();
    print("a b ", a, b)
    print(a.CGI_BIN_TOKEN.replace("APPID", a.APPID).replace("APPSECRET", a.APPSECRET));

    jsonT = '{"access_token":"ACCESS_TOKEN","expires_in":7200}';
    print("json1 -- ", jsonT);
    # jsonT = json.dumps(jsonT);

    jsonT = json.loads(jsonT);
    print("json2 -- ", jsonT["expires_in"]);
