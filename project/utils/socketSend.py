#!/usr/bin/env python
# -*- coding=utf-8 -*-


"""
file: send.py
socket client
"""

import socket
import os
import sys
import struct

'''
字符串和字节互转
'''
def convertStrOrByte():
    # bytes object
    byte = b"example"

    # str object
    str = "example"

    # str to bytes 字符串转字节
    bytes(str, encoding="utf8")

    # bytes to str  字节转字符串
    str(bytes, encoding="utf-8")

    # an alternative method
    # str to bytes  字符串转为字节
    str.encode(str)

    # bytes to str  字节转为字符串
    bytes.decode(bytes)


def socket_client():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(('127.0.0.1', 9000))
    except socket.error as msg:
        print(msg);
        sys.exit(1)

    print(s.recv(1024));

    while 1:
        #filepath = input('please input file path: ')
        filepath = "C:/Users/Administrator/Desktop/sort.exe";
        if os.path.isfile(filepath):
            # 定义定义文件信息。128s表示文件名为128bytes长，l表示一个int或log文件类型，在此为文件大小 2s 代表的是2个字节的字符串长度
            fileinfo_size = struct.calcsize('128si2s')
            # 定义文件头信息，包含文件名和文件大小
            fhead = struct.pack('128si2s', bytes(os.path.basename(filepath).encode('utf-8')),os.stat(filepath).st_size,b'pw');
            s.send(fhead)
            print('client filepath: {0}'.format(filepath))

            fp = open(filepath, 'rb')
            while 1:
                data = fp.read(1024)
                if not data:
                    print('{0} file send over...'.format(filepath));
                    break
                s.send(data)
        s.close()
        break


if __name__ == '__main__':
    socket_client()