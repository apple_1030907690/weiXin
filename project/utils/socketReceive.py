#!/usr/bin/env python
# -*- coding=utf-8 -*-


"""
file: recv.py
socket service
"""

import re
import socket
import threading
import time
import sys
import os
import struct


#读取配置的依赖
import re
import os
import tempfile


def socket_service():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('127.0.0.1', 9000))
        s.listen(10)
    except socket.error as msg:
        print(msg);
        sys.exit(1)
    print('Waiting client connection...');

    while 1:
        conn, addr = s.accept()
        t = threading.Thread(target=deal_data, args=(conn, addr))
        t.start()

def deal_data(conn, addr):
    print('Accept new connection from {0}'.format(addr));
    #conn.settimeout(500)
    conn.send(b'Hi, Welcome to the server!')

    while 1:
        fileinfo_size = struct.calcsize('128si2s')
        buf = conn.recv(fileinfo_size)
        if buf:
            filename, filesize ,pwd = struct.unpack('128si2s', buf);
            print("pwd : "+ bytes.decode(pwd));
            #判定密码是否正确
            file_path = './test.properties'
            property = Properties(file_path);  # 读取文件
            if property.get("pwd") == bytes.decode(pwd):
                print("password validate success!")
            else:
                print("password validate error")
                break;
            #删除byte转为str后的\x00  用strip也可以
            newFileName = bytes.decode(filename).rstrip('\x00');
            #得到文件路径前缀
            dirPrefix = property.get(newFileName);
            if dirPrefix == None or dirPrefix == "":
                dirPrefix = property.get("defaultDir");
            new_filename = os.path.join(dirPrefix, '' +newFileName);
            print('file new name is {0}, filesize if {1}'.format(new_filename,filesize));

            recvd_size = 0  # 定义已接收文件的大小
            fp = open(new_filename, 'wb')
            print('start receiving...');

            while not recvd_size == filesize:
                if filesize - recvd_size > 1024:
                    data = conn.recv(1024)
                    recvd_size += len(data)
                else:
                    data = conn.recv(filesize - recvd_size)
                    recvd_size = filesize
                fp.write(data)
            fp.close()
            print('end receive...');
        conn.close()
        break



'''

zhouzhongqing
2018年5月2日16:17:13
读取properties文件

'''



class Properties:

    def __init__(self, file_name):
        self.file_name = file_name
        self.properties = {}
        try:
            fopen = open(self.file_name, 'r')
            for line in fopen:
                line = line.strip()
                if line.find('=') > 0 and not line.startswith('#'):
                    strs = line.split('=')
                    self.properties[strs[0].strip()] = strs[1].strip()
        except Exception:
            #raise Exception
            print("read file exception ...");
        else:
            fopen.close();

    def has_key(self, key):
        return key in self.properties

    def get(self, key, default_value=''):
        if key in self.properties:
            return self.properties[key]
        return default_value

    def put(self, key, value):
        self.properties[key] = value
        replace_property(self.file_name, key + '=.*', key + '=' + value, True)


def parse(file_name):
    return Properties(file_name)


def replace_property(file_name, from_regex, to_str, append_on_not_exists=True):
    tmpfile = tempfile.TemporaryFile()

    if os.path.exists(file_name):
        r_open = open(file_name, 'r')
        pattern = re.compile(r'' + from_regex)
        found = None
        for line in r_open:
            if pattern.search(line) and not line.strip().startswith('#'):
                found = True
                line = re.sub(from_regex, to_str, line)
            tmpfile.write(bytes(line, encoding="utf8"))
        if not found and append_on_not_exists:
            tmpfile.write( bytes('\n' + to_str ,encoding="utf-8"))
        r_open.close()
        tmpfile.seek(0)

        content = tmpfile.read()

        if os.path.exists(file_name):
            os.remove(file_name)

        w_open = open(file_name, 'w')
        w_open.write(str(content,encoding="utf-8"))
        w_open.close()

        tmpfile.close()
    else:
        print("file %s not found" % file_name);



if __name__ == '__main__':
    socket_service()


