# -*- coding: utf-8 -*-

'''

关闭逻辑服
'''


import os


# execute command, and return the output
def execCmd(cmd):
    r = os.popen(cmd)
    text = r.read()
    r.close()
    return text


if __name__ == '__main__':
    print("shutdown start ");

    project_list = [ "game-bullfight-1.0.0", "game-dragontiger-1.0.0",
                    "game-fruit-machine-1.0.0", "game-glodflower-1.0.0", "game-gragontiles-1.0.0", "game-robtaurus-1.0.0", "game-baccara-1.0.0",
                    "game-cqssc-1.0.0", "game-fores-1.0.0", "game-gemstorm-1.0.0", "game-goodstart-1.0.0", "game-red-black-1.0.0"
                     ,"hall-server-1.0.0", "gate-server-1.0.0",  "db-server-1.0.0","center-server-1.0.0",];


    for project_item in project_list:
        procId = execCmd("ps -ef | grep "+project_item+" |grep -v 'grep' | awk '{print $2}' | head -1");
        if procId != None and "" != procId:
            print("project name -> " + project_item + " pid -> " + str(procId));
            os.system("kill -15 " + str(procId))
            print("runing kill -15 " + str(procId));
        else:
            print(project_item + " is shutdown status");


    print("shutdown end ");