# -*- coding: utf-8 -*-

import os

'''
启动游戏逻辑服务
'''


# execute command, and return the output
def execCmd(cmd):
    r = os.popen(cmd)
    text = r.read()
    r.close()
    return text


if __name__ == '__main__':
    print(" start game ");
    basePath = "/opt/new_project/";
    project_list = ["center-server","db-server","gate-server","hall-server","game-bullfight","game-dragontiger",
                    "game-fruit-machine","game-glodflower","game-gragontiles","game-robtaurus","game-baccara",
                    "game-cqssc","game-fores","game-gemstorm","game-goodstart","game-red-black"];



    if os.path.exists(basePath):
        for project_item in project_list:
            if os.path.exists(basePath + project_item):
                procId = execCmd("ps -ef | grep " + project_item + " |grep -v 'grep' | awk '{print $2}' | head -1");
                if procId != None and "" != procId:
                    print(" project  " + project_item + " is run status...")
                else:
                    print(" run cd " + basePath + project_item);
                    os.chdir(basePath + project_item);
                    print(" start  " + project_item);
                    os.system("./start.sh");
            else:
                print(basePath + project_item  + " not found.");
    else:
        print("basePath not found " + basePath);
    # 切换工作目录
    #os.chdir("/opt/new_project/login-server");
    #os.system("./start.sh");

    print(" end game ");
