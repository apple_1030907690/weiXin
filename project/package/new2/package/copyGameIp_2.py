# -*- coding: utf-8 -*-

'''
打包复制文件的脚本--- 新框架
'''

import os
import shutil

if __name__ == '__main__':
    print("copy game-ip start ");

    # 源路径
    origin_path = "D:/work/newFramework/east-meeting-chess-admin/trunk/east-meeting-chess-server/game-ip/";
    # copy到的路径
    target_path = "D:/work/package/212_test/learngit/game_package/game-ip/";
    target = "target/";
    jar_name = "game-ip-0.0.1-SNAPSHOT.jar";
    if False == os.path.exists(target_path):
        print("makedirs " + target_path);
        #创建多级目录
        os.makedirs(target_path);

    #如果存在jar先删除
    if os.path.exists( target_path + jar_name):
        print("delete file " +  target_path + jar_name);
        os.remove( target_path + jar_name);

    shutil.copy(origin_path + target + jar_name ,  target_path + jar_name);

    print("copy game-ip end ")
