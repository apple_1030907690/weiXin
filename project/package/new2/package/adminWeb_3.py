# -*- coding: utf-8 -*-

'''
打包复制文件的脚本--- 新框架
'''

import os
import shutil


'''
删除文件夹
'''

def del_folder(path):
    if os.path.exists(path):
        for i in os.listdir(path):
            path_file = os.path.join(path, i)  # 取文件绝对路径
            if os.path.exists(path_file):
                if os.path.isfile(path_file):
                    os.remove(path_file);
                    print("remove " + path_file);
                else:
                    shutil.rmtree(path_file)  # 递归删除文件夹
                    print("rmtree " + path_file);
    else:
        print(" not path" + path);


if __name__ == '__main__':
    print("copy web start ");
    random_input_profile = input(" 请输入环境. ");

    basePath = "D:/work/web-view/trunk/project_web/";
    dist = "dist";
    del_folder(basePath + dist);
    src = "src";
    assets = "assets";
    os.chdir(basePath);
    os.system("npm run build " + random_input_profile);

    os.mkdir(basePath + dist + "/" + src);

    shutil.copytree(basePath + src + "/" + assets, basePath + "/" + dist + "/" + src + "/" + assets);

    # 源路径
    origin_path = "D:/work/web-view/trunk/project_web/";
    # copy到的路径
    target_path = "D:/work/package/212_test/learngit/";

    web = "web";

    del_folder(target_path+ web);
    if False == os.path.exists(target_path+ web):
        os.makedirs(target_path+ web);


    for item in os.listdir(origin_path+ dist):
        print(item);
        if os.path.isfile(origin_path+ dist+"/"+item):
            shutil.copy(origin_path+dist+"/" + item,target_path+web+"/"+item);
        else:
            shutil.copytree(origin_path + dist + "/" + item, target_path + web + "/" + item);



    print("copy web end ")
