# -*- coding: utf-8 -*-

'''
替换配置文件 复制文件的脚本--- 新框架
'''

import os
import shutil

'''
删除path文件夹 下面的所有
'''


def del_folder(path):
    if os.path.exists(path):
        for i in os.listdir(path):
            path_file = os.path.join(path, i)  # 取文件绝对路径
            if os.path.exists(path_file):
                if os.path.isfile(path_file):
                    os.remove(path_file);
                    print("remove " + path_file);
                else:
                    shutil.rmtree(path_file)  # 递归删除文件夹
                    print("rmtree " + path_file);
    else:
        print(" not path " + path);


'''
初始化创建需要的文件夹
'''


def init_mkdir_folder(path, list_folder):
    for i in list_folder:
        path_file = os.path.join(path, i)  # 取文件绝对路径
        if False == os.path.exists(path_file):
            os.mkdir(path_file);


#需要执行的python文件
class PythonFile:
    def __init__(self, name, path,number):
        self.name = name;
        self.path = path;
        self.number = number;

    def getName(self):
        return self.name;

    def getPath(self):
        return self.path;


    def getNumber(self):
        return self.number;

    def __lt__(self, other):  # override <操作符
        if self.number < other.number:
            return True
        return False


if __name__ == '__main__':
    print("start");
    # 正式环境的配置文件
    replaceBasePath = "D:/work/package/temp_config/";
    # 需要替换的项目
    replaceArray = ["center-server", "manage-games"];

    # 要替换的项目地址
    replacePathArray = ["D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/",
                        "D:/work/newFramework/manage-games/trunk/"];
    target = "/target/";

    print(" start center-server replace")
    # center-server中心服
    res = "res/";
    i = 0;
    projectPath = replacePathArray[i] + replaceArray[i];
    files = replaceBasePath + replaceArray[i];
    for file in os.listdir(files):
        #print(file);
        if os.path.exists(replacePathArray[i] + replaceArray[i]+target+res+  file):
            os.remove(replacePathArray[i] + replaceArray[i]+target+res+  file)
            print(replacePathArray[i] + replaceArray[i]+target+res+  file + " exists remove");

        shutil.copy(replaceBasePath + replaceArray[i] + "/"+file,replacePathArray[i] + replaceArray[i]+target+res+ file );
        print(  " copy file " + replaceBasePath + replaceArray[i] + "/"+file);

    print(" end center-server replace")

    print(" \n\n");

    print(" start manage-games replace");
    # 后台控杀
    res = "res/";
    i = 1;
    projectPath = replacePathArray[i] + replaceArray[i];
    files = replaceBasePath + replaceArray[i];
    for file in os.listdir(files):
        #print(replacePathArray[i] + replaceArray[i] + target + replaceArray[i] + "/" +res + file);
        if os.path.exists(replacePathArray[i] + replaceArray[i] + target + replaceArray[i] + "/" +res + file):
            os.remove(replacePathArray[i] + replaceArray[i] + target + replaceArray[i] + "/" +res + file)
            print(replacePathArray[i] + replaceArray[i] + target + replaceArray[i] + "/" +res + file + " exists remove");

        shutil.copy(replaceBasePath + replaceArray[i] + "/" + file,  replacePathArray[i] + replaceArray[i] + target + replaceArray[i] + "/" +res + file);
        print(" copy file " + replaceBasePath + replaceArray[i] + "/" + file);


    print(" end manage-games replace");

    # ---------------------------------------------------下面是第二版2018年10月12日10:27:21  start --------------------------------------------------------------------
    # 排除执行的脚本文件
    exclude_project = ["replace_config_newFramework.py", "README.md"];
    pythonFileList = [];

    for py_file_name in os.listdir("./"):
        if py_file_name not in exclude_project:
            py_file_name_arr = py_file_name.split("_");

            # 文件的执行顺序
            invoke_number = 0;  # 默认使用0
            if len(py_file_name_arr) > 1:
                invoke_number = py_file_name_arr[1].strip(".py");
            pythonFile = PythonFile(py_file_name, "./" + py_file_name, int(invoke_number));
            pythonFileList.append(pythonFile);

    # 排序 这里是升序  python list对象排序 https://blog.csdn.net/lianshaohua/article/details/80483357
    pythonFileList.sort();
    for py_file_name_item in pythonFileList:
        print("invoke " + py_file_name_item.getName());
        os.system("python " + py_file_name_item.getName());

    # ---------------------------------------------------下面是第二版2018年10月12日10:27:21  end --------------------------------------------------------------------


    print("end");
    random_input = input("please random input over...");
