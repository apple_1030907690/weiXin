# -*- coding: utf-8 -*-

'''
打包配置 --- 新框架
'''

import os
import shutil

if __name__ == '__main__':
    print(" replaceAdminConfig start ");

    origin_path = "D:/work/newFramework/east-meeting-chess-admin/trunk/east-meeting-chess-server/";

    package_config = "D:/work/package/temp_config/";

    if False == os.path.exists(package_config) or False == os.path.exists(origin_path):
        print(package_config + " or " + origin_path + " path  not found ");


    print("\n\n");
    project_name = "server-admin";
    target = "target";
    root = "ROOT";
    WEB_INF = "WEB-INF";
    classes = "classes";
    for file in os.listdir(package_config + project_name):
        # print(file);
        checkPath = origin_path +project_name+"/"+target+"/"+root+"/"+WEB_INF+"/"+classes+"/"+ file;
        if os.path.exists(checkPath):
            print(checkPath+ " exists remove");
            os.remove(checkPath);
            shutil.copy(package_config + project_name+ "/"+ file ,checkPath );
            print(" copy file " + package_config + project_name + "/" + file);

    print("\n\n");

    project_name = "custom-chat";


    for file in os.listdir(package_config + project_name):
        checkPath = origin_path + project_name + "/" + target + "/" + root + "/" + WEB_INF + "/" + classes + "/" + file;
        if os.path.exists(checkPath):
            print(checkPath + " exists remove");
            os.remove(checkPath);
            shutil.copy(package_config +  project_name + "/" + file, checkPath);
            print(" copy file " + package_config + project_name + "/" + file);

    print("\n\n");
    config_path = "custom-chat/target/ROOT/config/";

    for file in os.listdir(package_config + project_name):
        checkPath = origin_path +  config_path + file;
        if os.path.exists(checkPath):
            print(checkPath + " exists remove");
            os.remove(checkPath);
            shutil.copy(package_config +  project_name + "/" + file, checkPath);
            print(" copy file " + package_config + project_name + "/" + file);


    print(" replaceAdminConfig end ");
