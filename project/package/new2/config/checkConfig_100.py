# -*- coding: utf-8 -*-

'''
打包配置100 --- 新框架
'''

import os
import shutil


# 读文件
def read_to_file(file_name):
    '''''
              讲txt文本存入到file_name文件中
          '''
    print(" current read file " + str(file_name));
    # 1 打开文件
    # w 如果没有这个文件将创建这个文件
    '''
    'r'：读

    'w'：写

    'a'：追加

    'r+' == r+w（可读可写，文件若不存在就报错(IOError)）

    'w+' == w+r（可读可写，文件若不存在就创建）

    'a+' ==a+r（可追加可写，文件若不存在就创建）
    '''
    f = open(file_name, 'r');

    line = f.read();
    # 3 关闭文件
    f.close()
    return line;

if __name__ == '__main__':
    print("checkConfig start ");

    origin_path = "D:/work/package/temp_config/";
    environmental = "";
    for file_name in os.listdir(origin_path):
        #print("  file " + file_name);
        if file_name.find(".txt") != -1:
            environmental = read_to_file(origin_path + file_name);
            print(" current profile :" + environmental);


    if "prod" == environmental:
        print(" 阿里云的正式环境 ");
    elif "prod_li" == environmental:
        print("李升林那边的正式环境");
    elif "dev" == environmental:
        print("222服务器的测试环境");
    else:
        print("环境未知,请检查...")
    print("checkConfig end ")
