# -*- coding: utf-8 -*-

'''
打包复制文件的脚本--- 新框架
'''

import os
import shutil

'''
删除文件夹
'''

def del_folder(path):
    if os.path.exists(path):
        for i in os.listdir(path):
            path_file = os.path.join(path, i)  # 取文件绝对路径
            if os.path.exists(path_file):
                if os.path.isfile(path_file):
                    os.remove(path_file);
                    print("remove " + path_file);
                else:
                    shutil.rmtree(path_file)  # 递归删除文件夹
                    print("rmtree " + path_file);
    else:
        print(" not path" + path);


'''
初始化创建需要的文件夹
'''


def init_mkdir_folder(path, list_folder):
    for i in list_folder:
        path_file = os.path.join(path, i)  # 取文件绝对路径
        if False == os.path.exists(path_file):
            os.mkdir(path_file);


if __name__ == '__main__':
    print("start");
    # 游戏项目的
    copyPath = "D:/work/package/212_test/learngit/game_package/";
    basePathPrefix = "D:/work/newFramework/east-meeting-chess-server/trunk/east-meeting-chess-server/";
    # 暂时不打  game-glodflower
    basePathProjectArray = ["center-server", "game-baccara", "db-server", "game-bullfight", "game-dragontiger",
                            "game-goodstart","game-glodflower", "game-red-black", "gate-server", "hall-server",
                            "login-server", "log-server","game-fores","game-gragontiles","game-robtaurus","game-cqssc","game-fruit-machine","game-gemstorm"];
    del_folder(copyPath);
    init_mkdir_folder(copyPath, basePathProjectArray);

    target = "/target/";
    jarSuffix = "-1.0.0.jar";
    res = "res";
    for ba in basePathProjectArray:
        print("copy game jar " + basePathPrefix + ba + target + ba + jarSuffix);
        shutil.copy(basePathPrefix + ba + target + ba + jarSuffix, copyPath + ba);
        #copy res
        shutil.copytree(basePathPrefix + basePathProjectArray[0] + target + res,  copyPath + ba+"/"+res);


    print("copy game done...");

    print("copy lib and libs start ...")
    lib = "lib";
    libs = "libs";
    shutil.copytree(basePathPrefix + basePathProjectArray[0] + target + lib,
                    copyPath + basePathProjectArray[0] + "/" + libs);
    shutil.copytree(basePathPrefix + basePathProjectArray[1] + target + lib,
                    copyPath + basePathProjectArray[0] + "/" + lib);
    print("copy lib and libs end ...")

    print(" copy admin_package start ");
    admin_package = "D:/work/package/212_test/learngit/admin_package/";
    admin_package_target = "D:/work/newFramework/east-meeting-chess-admin/trunk/east-meeting-chess-server/";
    server_admin = "server-admin";
    ROOT = "ROOT";
    del_folder(admin_package);
    shutil.copytree(admin_package_target + server_admin + target + ROOT, admin_package + ROOT);
    print(" copy admin_package end ");

    print("copy chat_package start ");
    chat_package = "D:/work/package/212_test/learngit/chat_package/";
    custom_chat = "custom-chat";
    del_folder(chat_package);
    shutil.copytree(admin_package_target + custom_chat + target + ROOT, chat_package + ROOT);
    print("copy chat_package end ");

    print("copy manage-games start ");
    manage_games_target = "D:/work/newFramework/manage-games/trunk/"
    manage_games = "manage-games";
    shutil.copytree(manage_games_target + manage_games + target + manage_games, chat_package + manage_games);
    print("copy manage-games end ");


    print("end");
    random_input = input(" 请输入任意字符结束. ");
