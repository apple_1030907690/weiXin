# -*- coding: utf-8 -*-

'''
mvn 打包的脚本
'''
import os
import time
import json



#配置文件的操作 start
import tempfile
import re

class Properties:

    def __init__(self, file_name):
        self.file_name = file_name
        self.properties = {}
        try:
            fopen = open(self.file_name, 'r')
            for line in fopen:
                line = line.strip()
                if line.find('=') > 0 and not line.startswith('#'):
                    strs = line.split('=')
                    self.properties[strs[0].strip()] = strs[1].strip()
        except Exception:
            #raise Exception
            print("read file exception ...");
        else:
            fopen.close();

    def has_key(self, key):
        return key in self.properties

    def get(self, key, default_value=''):
        if key in self.properties:
            return self.properties[key]
        return default_value

    def put(self, key, value):
        self.properties[key] = value
        replace_property(self.file_name, key + '=.*', key + '=' + value, True)


def parse(file_name):
    return Properties(file_name)


def replace_property(file_name, from_regex, to_str, append_on_not_exists=True):
    tmpfile = tempfile.TemporaryFile()

    if os.path.exists(file_name):
        r_open = open(file_name, 'r')
        pattern = re.compile(r'' + from_regex)
        found = None
        for line in r_open:
            if pattern.search(line) and not line.strip().startswith('#'):
                found = True
                line = re.sub(from_regex, to_str, line)
            tmpfile.write(bytes(line, encoding="utf8"))
        if not found and append_on_not_exists:
            tmpfile.write( bytes('\n' + to_str ,encoding="utf-8"))
        r_open.close()
        tmpfile.seek(0)

        content = tmpfile.read()

        if os.path.exists(file_name):
            os.remove(file_name)

        w_open = open(file_name, 'w')
        w_open.write(str(content,encoding="utf-8"))
        w_open.close()

        tmpfile.close()
    else:
        print("file %s not found" % file_name);


#配置文件的操作 end


if __name__ == '__main__':
    print("start");
    property = Properties("./sys.properties");  # 读取文件

    mvnPackageConfig = property.get("mvnPackageConfig");
    #project_option = ["real_external_network_test45","real_intranet188","dev","real_to_examine73"];
    mvnPackageConfig = json.loads(mvnPackageConfig);
    #print("mvnPackageConfig =" + str(mvnPackageConfig));
    project_option = [];
    #提示的字符串
    prompt_str = "Please enter ";
    #提示的下标
    num = 0;
    for mvnConf in mvnPackageConfig:
        #print(" key " + str(mvnConf["key"]) + " value " +  str(mvnConf["value"]) );
        project_option.append(str(mvnConf["key"]));
        prompt_str += " " + str(num) +"-" +str(mvnConf["value"])  + " ";
        num = num+1;


    #print("project_option " + str(project_option));

    user_input = input(prompt_str);

    print("invoke "+"mvn clean package -P"+project_option[int(user_input)]);

    time.sleep(3);
    os.system("mvn clean package -P"+project_option[int(user_input)]);

    random_input = input(" 请输入任意字符结束. ");


    print("end");
