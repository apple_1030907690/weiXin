# -*- coding: utf-8 -*-

'''
mvn 打包的脚本
'''
import os
import shutil


def del_folder(path):
    for i in os.listdir(path):
        path_file = os.path.join(path, i)  # 取文件绝对路径
        if os.path.exists(path_file):
            if os.path.isfile(path_file):
                os.remove(path_file);
                print("remove " + path_file);
            else:
                shutil.rmtree(path_file)  # 递归删除文件夹
                print("rmtree " + path_file);


def read_file(path,result):

    file = open(path, 'rb');

    while 1:
        line = file.readline();
        #print("line " + str(line));
        if str(line).find(result) != -1:
            print("配置有错,请检查配置 "+  path+" \t\t" + result);
        if not line:
            break
        pass  # do something


if __name__ == '__main__':
    print("start");
    dict = {'project_web': "// base: '/web/'",
            'agent_mobile': "// base: '/mobile/'",
            'agent_web': "// base: '/agent/'",
            "channel_web":"// base: '/web/'",
            "chat_web":"// base: '/chat/'"
            }
    #basePath = "F:\\work\\red_package\\redpacket-web\\project_web\\";
    basePath = os.getcwd()+"/";
    result = "";
    for key in dict:
        if basePath.find(key) != -1:
            # 找到key
            result = dict[key];

    # 判断是否被屏蔽
    indexJs = "src/router/index.js";
    read_file(basePath+indexJs,result);

    dist = "dist";
    # 如果没有则创建
    if False == os.path.exists(basePath + dist):
        print("创建文件夹 " + dist)
        os.mkdir(basePath + dist)
    del_folder(basePath + dist);
    random_input_profile = input(" 请输入环境. ");
    src = "src";
    assets = "assets";
    os.system("npm run build " + random_input_profile);

    os.mkdir(basePath + dist + "/" + src);

    shutil.copytree(basePath + src + "/" + assets, basePath + "/" + dist + "/" + src + "/" + assets);

    print("end");
    random_input = input(" 请输入任意字符结束. ");
