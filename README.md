Django微信公众号开发(没有企业号很多功能没有权限已停止更新)


运行需要的模块： 

    名称   下载地址
    lxml  https://pypi.python.org/pypi/lxml/2.3/
    requests https://pypi.python.org/pypi/requests或者https://github.com/kennethreitz/requests
    Django https://www.djangoproject.com/download/
    PyMySQL  https://pypi.python.org/pypi/PyMySQL#downloads



运行:

    python3.5 manage.py runserver 0.0.0.0:8083 #0.0.0.0让其它电脑可连接到开发服务器，8000为端口号。如果不指定，那么端口号默认为8000
    python3.5 manage.py runserver 0.0.0.0:8083 1>/root/app/logs/weixin.log 2>&1 &  #这是后台运行输出日志文件到/root/app/logs/weixin.log



